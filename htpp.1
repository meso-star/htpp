.\" Copyright (C) 2018-2020, 2023 |Méso|Star> (contact@meso-star.com)
.\" Copyright (C) 2018, 2019 Centre National de la Recherche Scientifique
.\" Copyright (C) 2018, 2019 Université Paul Sabatier
.\"
.\" This program is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.Dd September 11, 2023
.Dt HTPP 1
.Os
.Sh NAME
.Nm htpp
.Nd post-processing of
.Xr htrdr-image 5
images
.Sh SYNOPSIS
.Nm
.Op Fl fhVv
.Op Fl i Ar image_option Ns Op : Ns Ar image_option ...
.Op Fl m Ar map_option Ns Op : Ns Ar map_option ...
.Op Fl o Ar output
.Op Fl t Ar threads_count
.Op Ar input
.Sh DESCRIPTION
.Nm
post-processes a
.Xr htrdr-image 5
and converts it into a PPM image or
.Xr gnuplot 1
script.
.Pp
If
.Ar input
is not set, the image to post-process is read from standard input.
Similarly, if the
.Ar output
file is not defined, then the result is written to the standard output.
.Pp
Two post-processing procedures are provided: image color post-processing
.Pq option Fl i
and mapping of a given pixel component to a color gradient
.Pq option Fl m .
By default,
.Nm
post-processes the image color.
.Pp
The options are as follows:
.Bl -tag -width Ds
.It Fl f
Force overwriting of
.Ar output
file.
.It Fl h
Display short help.
.It Fl i Ar image_option Ns Op : Ns Ar image_option ...
Color post-processing.
The first third and fifth components of each pixel of the input
.Xr htrdr-image 5
are assumed to encode a color in the CIE 1931 XYZ color space.
The colors are first tone-mapped as follows:
.Pp
.Dl out_color = f Ns Po in_color * exposure Pc / f Ns Po white * exposure Pc
.Pp
with:
.Bd -literal -offset Ds
.No f Ns Po Va x Pc = Po Va x Ns * Ns Po A* Ns Va x No + C*B Pc + D*E Pc Ns \
/ Ns Po Va x Ns * Ns Po A* Ns Va x No + B + D*F Pc Pc - E/F
A = 0.15
B = 0.50
C = 0.10
D = 0.20
E = 0.02
F = 0.30
.Ed
.Pp
Exposure and white color values are user settings
.Pq see below .
Once tone-mapped, pixels are transformed from CIE 1931 XYZ color space to
linear sRGB color space before being gamma-corrected.
Finally, the resulting pixel components are truncated between
.Bq 0, 1
before being encoded on 8 bits.
.Pp
The post-processing options are as follows:
.Bl -tag -width Ds
.It Cm default
Use default options.
.It Cm exposure= Ns Ar real
Pixel exposure.
Its default value is 1.
.It Cm white= Ns Ar radiance
Radiance value representing white color (in W/sr/m^2).
If not defined, it is set such that 99.5% of the input pixel radiance is less
than its value.
.El
.It Fl m Ar map_option Ns Op : Ns Ar map_option ...
Matching pixel component to a color gradient.
The component to be post-processed is defined by the
.Cm pixcpnt
option.
The component is normalized according to its range over the whole image, or a
user-defined range if defined.
It is finally converted into a color gradient whose name is defined by the
.Cm palette
option.
.Pp
The color matching options are as follows:
.Bl -tag -width Ds
.It Cm default
Use default options.
.It Cm palette= Ns Ar palette_name
Palette to use.
If it is not defined, it takes on the inferno value.
The valid palette names are:
.Pp
.Bl -item -compact -offset Ds
.It
accent
.It
blues
.It
brbg
.It
bugn
.It
bupu
.It
chromajs
.It
dark2
.It
gnbu
.It
gnpu
.It
greens
.It
greys
.It
inferno
.It
jet
.It
magma
.It
moreland
.It
oranges
.It
orrd
.It
paired
.It
parula
.It
pastel1
.It
pastel2
.It
piyg
.It
plasma
.It
prgn
.It
pubu
.It
pubugn
.It
puor
.It
purd
.It
purples
.It
rdbu
.It
rdgy
.It
rdpu
.It
rdylbu
.It
rdylgn
.It
reds
.It
sand
.It
set1
.It
set2
.It
set3
.It
spectral
.It
viridis
.It
whgnbu
.It
whylrd
.It
ylgn
.It
ylgnbu
.It
ylorbr
.It
ylorrd
.It
ylrd
.El
.It Cm pixcpnt= Ns Ar pixel_component
Index of the pixel component to be be mapped.
It must lie within the range
.Bq 0, 7 .
By default it is set to 0, i.e. the first pixel component.
.It Cm range= Ns Ar min , Ns Ar max
Range ov values to be mapped.
A degenerated range
.Pq i.e. Ar min No >= Ar max
means that the range is that of the pixel component over the whole image.
This is the default behavior.
.It Cm gnuplot
Write an output gnuplot script that generates a PNG image with a built-in color
ramp.
.El
.It Fl o Ar output
Output file.
If not defined, data is written to standard output.
.It Fl t Ar threads_count
Indication of the number of threads to be used.
By default,
.Nm
uses many threads as processor cores.
.It Fl v
Make
.Nm
verbose.
Messages are printed on the standard error.
When used as part of pixel color post-processing
.Pq option Fl i ,
.Nm
displays the radiance of the white color in the output image.
When mapping a pixel component to a color gradient
.Pq option Fl m ,
it displays the color gradient and its associated values.
.It Fl V
Display the version number and exit.
.El
.Sh EXIT STATUS
.Ex -std
.Sh EXAMPLES
Post-process
.Pa img.htrdr
and write the resulting PPM image to
.Pa img.ppm .
Use the
.Fl f
option to overwrite
.Pa img.ppm
if the file already exists.
Use an exposure of 0.2 and explicitly set the white color to 31.2 W/sr/m^2:
.Pp
.Dl htpp -i exposure=0.2:white=31.2 -fo img.ppm img.htrdr
.Pp
Clamp the values of the second pixel component in
.Bq 0, 2
and map the result to the magma color ramp:
.Pp
.Dl htpp -vm pixcpnt=1:palette=magma:range=0,2 -fo map.ppm img.htrdr
.Pp
Same as above, but use
.Xr gnuplot 1
to embed the magma color ramp in the output
.Pa map.png
file:
.Bd -literal -offset Ds
htpp -vm pixcpnt=1:palette=magma:range=0,2:gnuplot img.htrdr \\
| gnuplot - > map.png
.Ed
.Sh SEE ALSO
.Xr gnuplot 1 ,
.Xr htrdr-image 5 ,
.Xr ppm 5
.Sh HISTORY
.Nm
was originally developed to post-process images produced by
.Xr htrdr 1 .
