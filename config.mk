VERSION_MAJOR = 0
VERSION_MINOR = 5
VERSION_PATCH = 0
VERSION = $(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_PATCH)
PREFIX = /usr/local

LIB_TYPE = SHARED
#LIB_TYPE = STATIC

BUILD_TYPE = RELEASE
#BUILD_TYPE = DEBUG

################################################################################
# Tools
################################################################################
CC = cc
PKG_CONFIG = pkg-config

################################################################################
# Dependencies
################################################################################
PCFLAGS_SHARED =
PCFLAGS_STATIC = --static
PCFLAGS = $(PCFLAGS_$(LIB_TYPE))

SCMAP_VERSION = 0.1
SCMAP_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags scmap)
SCMAP_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs scmap)

RSYS_VERSION = 0.14
RSYS_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rsys)
RSYS_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rsys)

DPDC_CFLAGS = $(SCMAP_CFLAGS) $(RSYS_CFLAGS) -fopenmp
DPDC_LIBS = $(SCMAP_LIBS) $(RSYS_LIBS) -fopenmp -lm

################################################################################
# Compilation options
################################################################################
WFLAGS =\
 -Wall\
 -Wcast-align\
 -Wconversion\
 -Wextra\
 -Wmissing-declarations\
 -Wmissing-prototypes\
 -Wshadow

# Increase security/robustness of the generated binaries
CFLAGS_HARDENED =\
 -D_FORTIFY_SOURCES=2\
 -fcf-protection=full\
 -fPIE\
 -fstack-clash-protection\
 -fstack-protector-strong

CFLAGS_COMMON =\
 -std=c89\
 -pedantic\
 -fvisibility=hidden\
 -fstrict-aliasing\
 $(CFLAGS_HARDENED)\
 $(WFLAGS)

CFLAGS_RELEASE = -O2 -DNDEBUG $(CFLAGS_COMMON)
CFLAGS_DEBUG = -g $(CFLAGS_COMMON)
CFLAGS = $(CFLAGS_$(BUILD_TYPE))

################################################################################
# Linker options
################################################################################
LDFLAGS_HARDENED = -pie -Wl,-z,relro,-z,now

LDFLAGS_DEBUG =
LDFLAGS_RELEASE = -s
LDFLAGS = $(LDFLAGS_$(BUILD_TYPE)) $(LDFLAGS_HARDENED)
