# Copyright (C) 2018-2020, 2023 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2018, 2019 Centre National de la Recherche Scientifique
# Copyright (C) 2018, 2019 Université Paul Sabatier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

################################################################################
# Library building
################################################################################
SRC = src/htpp.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) htpp

$(DEP) $(OBJ): config.mk

htpp: $(OBJ)
	$(CC) $(CFLAGS) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS) $(DPDC_LIBS)

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(SCMAP_VERSION) scmap; then \
	  echo "scmap $(SCMAP_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

src/htpp.d: src/htpp_version.h

src/htpp_version.h: src/htpp_version.h.in config.mk
	sed -e 's#@VERSION_MAJOR@#$(VERSION_MAJOR)#g' \
	    -e 's#@VERSION_MINOR@#$(VERSION_MINOR)#g' \
	    -e 's#@VERSION_PATCH@#$(VERSION_PATCH)#g' \
	    src/htpp_version.h.in > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS) $(DPDC_CFLAGS) -c $< -o $@

################################################################################
# Installation
################################################################################
install: build_library
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/bin" htpp
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/htpp" COPYING README.md
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man1" htpp.1

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/bin/htpp"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/htpp/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/htpp/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man1/htpp.1"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library

clean:
	rm -f $(OBJ) htpp .config

distclean: clean
	rm -f $(DEP) src/htpp_version.h

lint:
	shellcheck -o all make.sh
	mandoc -Tlint -Wall htpp.1 || [ $$? -le 1 ]
